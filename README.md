Terraform Module
===========

[![GitLabCI](https://gitlab.com/mikesupertrampsters/terraform-modules/badges/master/pipeline.svg)](https://gitlab.com/mikesupertrampsters/terraform-modules)
<a href="https://codeclimate.com/github/mikesupertrampster/terraform-modules/maintainability"><img src="https://api.codeclimate.com/v1/badges/19de0be80feedd25e9ca/maintainability" /></a>

Modules in Terraform are self-contained packages of Terraform configurations that are managed as a group. Modules are used to create reusable components in Terraform as well as for basic code organization.

| Provider   | Module               | Description  |
| :--------: |--------------------- | ------------ |
| aws        | iam/assumable-role   | Creates an assumable iam role. |
| bundles    | terraform            | Creates necessary terraform state s3 bucket and dynamodb lock table. |
