#!/usr/bin/env bash
set -euo pipefail

module_dir=${1:-"."}
error_occured=0
summary=""
counter=0

error="\033[31mERROR\033[0m"
failure="\033[31mFAILURE\033[0m"
warning="\033[33mWARNING\033[0m"
unknown="\033[31mUNKNOWN\033[0m"

collate_summary() {
  location="${1}"
  status="${2}"
  output="${3}"
  severity=""

  case "${status}" in
    1)
      severity="${error}"
      error_occured=1
      ;;
    2)
      severity="${failure}"
      error_occured=1
      ;;
    3)
      case "${output}" in
        *ERROR* )
          error_occured=1
          severity="${error}"
          ;;
        *WARNING* )
          severity="${warning}"
          ;;
        *NOTICE* )
          severity="${unknown}"
          ;;
      esac
      ;;
    *)
      severity="${unknown}"
      error_occured=1
      ;;
  esac
  summary="${summary}\n    ${location}: ${severity} (${output})"
}

scan_modules() {
  module_list=$(find ${module_dir} -name '*.tf' ! -path "*.terraform*" -print0 | xargs -0 -n1 dirname | sort -u)

  while read directory; do
    pushd "${directory}" > /dev/null
    terraform get > /dev/null

    set +e
    lint_output=$(tflint --quiet --error-with-issues 2>&1)
    ret=$?
    set -e

    if [[ "x${ret}" != "x0" && "${lint_output}" != *"not found. Did you run"* ]]; then
      collate_summary "${directory}" "${ret}" "(${lint_output})"
    fi

    let "counter+=1"
    popd > /dev/null
  done <<< "${module_list}"

  if [[ "x${error_occured}" != "x0" ]]; then
    echo "#------------------------------------------------------------------------------"
    echo " Summary (scanned ${counter} modules):"
    echo "#------------------------------------------------------------------------------"
    echo -e "${summary}"
    exit 0
  else
    echo "All good :)"
  fi
}

scan_modules
