variable dns_hz_id {}
variable domain_name {}
variable tags { type = "map" }

output arn { value = "${aws_acm_certificate.certificate.arn}" }