#------------------------------------------------------------------------------
# general
#------------------------------------------------------------------------------
variable env {}
variable project {}
variable tags { type = "map", default = {} }

#------------------------------------------------------------------------------
# role
#------------------------------------------------------------------------------
variable assume_role_policy {}
variable attach_policies { type = "list", default = [] }
variable create_role { default = false }
variable max_session_duration { default = 3600 }
variable name {}
variable policy {}

#------------------------------------------------------------------------------
# outputs
#------------------------------------------------------------------------------
output arn  { value = "${aws_iam_role.role.*.arn[0]}" }
output name { value = "${aws_iam_role.role.*.name[0]}" }
