locals {
  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Managed_by  = "terraform"
  }
}

resource "aws_iam_policy" "policy" {
  count  = "${var.create_role ? 1 : 0}"
  name   = "${var.name}"
  policy = "${var.policy}"
}

resource "aws_iam_role" "role" {
  count                = "${var.create_role ? 1 : 0}"
  assume_role_policy   = "${var.assume_role_policy}"
  name                 = "${var.name}"
  max_session_duration = "${var.max_session_duration}"
  tags                 = "${merge(local.tags, var.tags)}"
}

resource "aws_iam_role_policy_attachment" "attachment" {
  count      = "${var.create_role ? 1 : 0}"
  policy_arn = "${aws_iam_policy.policy.arn}"
  role       = "${aws_iam_role.role.name}"
}

resource "aws_iam_role_policy_attachment" "additional" {
  count      = "${var.create_role ? length(var.attach_policies) : 0}"
  policy_arn = "${element(var.attach_policies, count.index)}"
  role       = "${aws_iam_role.role.name}"
}
