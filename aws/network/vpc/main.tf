locals {
  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Managed_by  = "terraform"
  }
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "1.64.0"

  name = "${var.name}"
  cidr = "${var.vpc_cidr}"

  azs             = "${var.azs}"
  private_subnets = "${var.private_subnets}"
  public_subnets  = "${var.public_subnets}"

  create_database_subnet_group = "${var.create_database_subnet_group}"
  enable_nat_gateway = "${var.enable_nat_gateway}"
  single_nat_gateway = "${var.single_nat_gateway}"

  tags = "${local.tags}"
}
