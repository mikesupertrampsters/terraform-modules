#------------------------------------------------------------------------------
# general
#------------------------------------------------------------------------------
variable env {}
variable name {}
variable project {}

#------------------------------------------------------------------------------
# vpc
#------------------------------------------------------------------------------
variable azs { type = "list" }
variable create_database_subnet_group { default = true }
variable enable_nat_gateway { default = true }
variable private_subnets { type = "list" }
variable public_subnets { type = "list" }
variable single_nat_gateway { default = true }
variable vpc_cidr {}

#------------------------------------------------------------------------------
# outputs
#------------------------------------------------------------------------------
output igw_id                   { value = "${module.vpc.igw_id}" }
output natgw_ids                { value = "${module.vpc.natgw_ids}" }
output nat_ids                  { value = "${module.vpc.nat_ids}" }
output nat_public_ips           { value = "${module.vpc.nat_public_ips}" }
output private_route_table_ids  { value = "${module.vpc.private_route_table_ids}" }
output private_subnets          { value = "${module.vpc.private_subnets}" }
output public_route_table_ids   { value = "${module.vpc.public_route_table_ids}" }
output public_subnets           { value = "${module.vpc.public_subnets}" }
output vpc_cidr_block           { value = "${module.vpc.vpc_cidr_block}" }
output vpc_id                   { value = "${module.vpc.vpc_id}" }
