#------------------------------------------------------------------------------
# general
#------------------------------------------------------------------------------
variable tags { type = "map", default = {} }
variable name_prefix {}
variable vpc_id {}

#------------------------------------------------------------------------------
# target group
#------------------------------------------------------------------------------
variable autoscaling_group_name {}
variable deregistration_delay { default = 60 }
variable tg_port {}
variable tg_protocol { default = "TLS" }
variable proxy_protocol_v2 { default = false }
variable tg_hc_path {}

#------------------------------------------------------------------------------
# load balancer
#------------------------------------------------------------------------------
variable enable_cross_zone_load_balancing { default = true }
variable fqdn {}
variable lb_internal { default = true }
variable subnets { type = "list" }

#------------------------------------------------------------------------------
# listener & acm
#------------------------------------------------------------------------------
variable dns_hz_id {}
variable listener_port { default = 443 }