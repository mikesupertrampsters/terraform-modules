resource "aws_lb_target_group" "tg" {
  lifecycle { create_before_destroy = true }

  deregistration_delay = "${var.deregistration_delay}"
  name_prefix          = "${var.name_prefix}"
  port                 = "${var.tg_port}"
  protocol             = "${var.tg_protocol}"
  proxy_protocol_v2    = "${var.proxy_protocol_v2}"
  vpc_id               = "${var.vpc_id}"

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    path                = "${var.tg_hc_path}"
    protocol            = "HTTPS"
    port                = "traffic-port"
    interval            = 10
  }

  tags = "${var.tags}"
}

resource "aws_autoscaling_attachment" "attachment" {
  alb_target_group_arn   = "${aws_lb_target_group.tg.arn}"
  autoscaling_group_name = "${var.autoscaling_group_name}"
}

resource "aws_lb" "nlb" {
  enable_cross_zone_load_balancing = "${var.enable_cross_zone_load_balancing}"
  internal           = "${var.lb_internal}"
  load_balancer_type = "network"
  name_prefix        = "${var.name_prefix}"
  subnets            = ["${var.subnets}"]
  tags               = "${var.tags}"
}

module "acm" {
  source = "../../../aws/acm/certificate"

  domain_name = "${var.fqdn}"
  tags        = "${var.tags}"
  dns_hz_id   = "${var.dns_hz_id}"
}

resource "aws_lb_listener" "listr" {
  load_balancer_arn = "${aws_lb.nlb.arn}"
  port              = "${var.listener_port}"
  protocol          = "TLS"
  certificate_arn   = "${module.acm.arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.tg.arn}"
    type             = "forward"
  }
}

resource "aws_route53_record" "record" {
  zone_id = "${var.dns_hz_id}"
  name    = "${var.fqdn}"
  type    = "A"

  alias {
    name                   = "${aws_lb.nlb.dns_name}"
    zone_id                = "${aws_lb.nlb.zone_id}"
    evaluate_target_health = true
  }
}
