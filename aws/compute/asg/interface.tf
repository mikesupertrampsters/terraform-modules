#------------------------------------------------------------------------------
# general
#------------------------------------------------------------------------------
variable azs { type = "list" }
variable env {}
variable project {}
variable region {}
variable stack {}
variable tags { type = "map", default = {} }

#------------------------------------------------------------------------------
# role
#------------------------------------------------------------------------------
variable assume_role_policy {}
variable iam_role_policy {}

#------------------------------------------------------------------------------
# asg
#------------------------------------------------------------------------------
variable cf_type { default = "asg-rolling" }
variable desired_capacity {}
variable max_size {}
variable min_size {}
variable pause_time { default = "PT15M" }
variable sg { default = "" }
variable vpc_zone_identifier { type = "list" }

#------------------------------------------------------------------------------
# ec2
#------------------------------------------------------------------------------
variable associate_public_ip_address { default = "false" }
variable enable_all_egress { default = "false" }
variable image_id {}
variable instance_type {}
variable key_name {}
variable root_volume_size {}
variable root_volume_type { default = "gp2" }
variable user_data {}
variable vpc_id {}

#------------------------------------------------------------------------------
# outputs
#------------------------------------------------------------------------------
output asg                  { value = "${aws_cloudformation_stack.stack.outputs["asg"]}" }
output cf_stack             { value = "${aws_cloudformation_stack.stack.name}" }
output instance_profile_arn { value = "${aws_iam_instance_profile.instance_profile.arn}" }
output role_arn             { value = "${aws_iam_role.iam_role.arn}" }
output role_name            { value = "${aws_iam_role.iam_role.name}" }
