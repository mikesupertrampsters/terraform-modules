locals {
  name = "${var.project}-${var.stack}-${var.env}"
  iam_name = "${title(var.project)}${title(var.stack)}${title(var.env)}"

  tags = {
    Name        = "${local.name}"
    Project     = "${var.project}"
    Environment = "${var.env}"
    Managed_by  = "terraform"
  }
}

#------------------------------------------------------------------------------
# role
#------------------------------------------------------------------------------
resource "aws_iam_role" "iam_role" {
  assume_role_policy = "${var.assume_role_policy}"
  name               = "${local.iam_name}"
}

resource "aws_iam_role_policy" "role_policy" {
  name   = "${aws_iam_role.iam_role.name}"
  policy = "${var.iam_role_policy}"
  role   = "${aws_iam_role.iam_role.id}"
}

resource "aws_iam_instance_profile" "instance_profile" {
  name = "${aws_iam_role.iam_role.name}"
  role = "${aws_iam_role.iam_role.name}"
}

#------------------------------------------------------------------------------
# asg & lc
#------------------------------------------------------------------------------
resource "aws_launch_configuration" "lc" {
  lifecycle { create_before_destroy = true }

  associate_public_ip_address = "${var.associate_public_ip_address}"
  iam_instance_profile        = "${aws_iam_instance_profile.instance_profile.name}"
  image_id                    = "${var.image_id}"
  instance_type               = "${var.instance_type}"
  key_name                    = "${var.key_name}"
  name_prefix                 = "${local.name}"
  security_groups             = ["${var.sg}"]
  user_data                   = "${var.user_data}"

  root_block_device {
    volume_size = "${var.root_volume_size}"
    volume_type = "${var.root_volume_type}"
  }
}

data "template_file" "cloudformation" {
  template = "${file("${path.module}/files/cloudformation/${var.cf_type}.yml")}"

  vars {
    aws_launch_configuration_id = "${aws_launch_configuration.lc.id}"
    azs                         = "${join(",", var.azs)}"
    desired_capacity            = "${var.desired_capacity}"
    environment                 = "${var.env}"
    min_instances_in_service    = "${var.min_size}"
    min_size                    = "${var.min_size}"
    max_size                    = "${var.max_size}"
    name                        = "${local.name}"
    pause_time                  = "${var.pause_time}"
    project                     = "${var.project}"
    vpc_zone_identifier         = "${join(",", var.vpc_zone_identifier)}"
  }
}

resource "aws_cloudformation_stack" "stack" {
  name          = "${local.name}"
  template_body = "${data.template_file.cloudformation.rendered}"
  tags          = "${merge(local.tags, var.tags)}"
}
