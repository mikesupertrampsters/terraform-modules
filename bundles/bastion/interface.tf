#------------------------------------------------------------------------------
# general
#------------------------------------------------------------------------------
variable env {}
variable project {}
variable region {}
variable tags { type = "map", default = {} }

#------------------------------------------------------------------------------
# asg
#------------------------------------------------------------------------------
variable azs { type = "list" }
variable dns_domain {}
variable dns_hz_id {}
variable iam_query_role {}
variable image_id {}
variable instance_type {}
variable key_name {}
variable max_size {}
variable min_size {}
variable root_volume_size { default = 100 }
variable vpc_id {}
variable vpc_zone_identifier { type = "list" }
