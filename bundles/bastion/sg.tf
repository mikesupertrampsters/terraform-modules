resource "aws_security_group" "sg" {
  lifecycle { create_before_destroy = true }
  name        = "${local.name}-sg"
  vpc_id      = "${var.vpc_id}"
  tags        = "${merge(local.tags, var.tags)}"
}

resource "aws_security_group_rule" "egress" {
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  security_group_id = "${aws_security_group.sg.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  type              = "egress"
}

resource "aws_security_group_rule" "ssh" {
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = "${aws_security_group.sg.id}"
  cidr_blocks       = ["0.0.0.0/0"]
  type              = "ingress"
}
