locals {
  name = "${var.project}-bastion-${var.env}"

  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Managed_by  = "terraform"
  }
}

data "template_file" "policy" {
  template = "${file("${path.module}/files/policy/ec2.json")}"

  vars {
    aws_sts_iam_query_role_arn = "${var.iam_query_role}"
  }
}

data "template_file" "cloudinit" {
  template = "${file("${path.module}/files/userdata/cloudinit.yml")}"

  vars {
    aws_sts_iam_query_role_arn  = "${var.iam_query_role}"
    role                        = "bastion"
  }
}

module "bastion" {
  source = "../../aws/compute/asg"

  assume_role_policy  = "${file("${path.module}/files/role/assume-role-ec2.json")}"
  azs                 = "${var.azs}"
  desired_capacity    = "${var.min_size}"
  env                 = "${var.env}"
  iam_role_policy     = "${data.template_file.policy.rendered}"
  image_id            = "${var.image_id}"
  instance_type       = "${var.instance_type}"
  key_name            = "${var.key_name}"
  max_size            = "${var.max_size}"
  min_size            = "${var.min_size}"
  project             = "${var.project}"
  region              = "${var.region}"
  root_volume_size    = "${var.root_volume_size}"
  sg                  = "${aws_security_group.sg.id}"
  stack               = "bastion"
  user_data           = "${data.template_file.cloudinit.rendered}"
  vpc_id              = "${var.vpc_id}"
  vpc_zone_identifier = "${var.vpc_zone_identifier}"
}
