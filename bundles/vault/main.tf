locals {
  name = "${var.project}-vault-${var.env}"
  fqdn = "vault.${replace(var.dns_domain, "/[.]$/", "")}"

  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Managed_by  = "terraform"
  }
}

data "template_file" "policy" {
  template = "${file("${path.module}/files/policy/ec2.json")}"

  vars {
    aws_sts_iam_query_role_arn = "${var.iam_query_role}"
    dynamodb_arns              = "${join("\",\"", list(aws_dynamodb_table.storage.arn))}"
    kms_arns                   = "${join("\",\"", list(aws_kms_key.unseal.arn))}"
  }
}

data "template_file" "cloudinit" {
  template = "${file("${path.module}/files/userdata/cloudinit.yml")}"

  vars {
    aws_default_region          = "${var.region}"
    aws_sts_iam_query_role_arn  = "${var.iam_query_role}"
    aws_dynamodb_table          = "${aws_dynamodb_table.storage.id}"
    vault_fqdn                  = "${local.fqdn}"
    vault_unseal_key_id         = "${aws_kms_key.unseal.id}"
  }
}

module "vault" {
  source = "../../aws/compute/asg"

  assume_role_policy  = "${file("${path.module}/files/role/assume-role-ec2.json")}"
  azs                 = "${var.azs}"
  desired_capacity    = "${var.min_size}"
  env                 = "${var.env}"
  iam_role_policy     = "${data.template_file.policy.rendered}"
  image_id            = "${var.image_id}"
  instance_type       = "${var.instance_type}"
  key_name            = "${var.key_name}"
  max_size            = "${var.max_size}"
  min_size            = "${var.min_size}"
  project             = "${var.project}"
  region              = "${var.region}"
  root_volume_size    = "${var.root_volume_size}"
  sg                  = "${aws_security_group.sg.id}"
  stack               = "vault"
  user_data           = "${data.template_file.cloudinit.rendered}"
  vpc_id              = "${var.vpc_id}"
  vpc_zone_identifier = "${var.vpc_zone_identifier}"
}

module "vault-nlb" {
  source = "../../aws/network/nlb"

  autoscaling_group_name = "${module.vault.asg}"
  dns_hz_id              = "${var.dns_hz_id}"
  fqdn                   = "${local.fqdn}"
  name_prefix            = "v-${var.env}"
  subnets                = "${var.vpc_zone_identifier}"
  tags                   = "${local.tags}"
  tg_hc_path             = "/v1/sys/health?standbyok=true"
  tg_port                = 8200
  vpc_id                 = "${var.vpc_id}"
}