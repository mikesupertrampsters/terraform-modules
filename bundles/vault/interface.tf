#------------------------------------------------------------------------------
# general
#------------------------------------------------------------------------------
variable env {}
variable project {}
variable region {}
variable tags { type = "map", default = {} }

#------------------------------------------------------------------------------
# backend
#------------------------------------------------------------------------------
variable vault_storage_read_capacity  { default = 1 }
variable vault_storage_write_capacity { default = 1 }

#------------------------------------------------------------------------------
# asg
#------------------------------------------------------------------------------
variable azs { type = "list" }
variable dns_domain {}
variable dns_hz_id {}
variable iam_query_role {}
variable image_id {}
variable instance_type {}
variable key_name {}
variable max_size {}
variable min_size {}
variable root_volume_size { default = 100 }
variable vpc_id {}
variable vpc_zone_identifier { type = "list" }

#------------------------------------------------------------------------------
# outputs
#------------------------------------------------------------------------------
output asg                  { value = "${module.vault.asg}" }
output cf_stack             { value = "${module.vault.cf_stack}" }
output instance_profile_arn { value = "${module.vault.instance_profile_arn}" }
output role_arn             { value = "${module.vault.role_arn}" }
output role_name            { value = "${module.vault.role_name}" }