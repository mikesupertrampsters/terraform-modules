resource "aws_kms_key" "unseal" {
  description             = "${local.name}-unseal-key"
  deletion_window_in_days = 10
  tags                    = "${merge(local.tags, var.tags)}"
}

resource "aws_dynamodb_table" "storage" {
  name           = "${local.name}-storage"
  hash_key       = "Path"
  range_key      = "Key"
  read_capacity  = "${var.vault_storage_read_capacity}"
  write_capacity = "${var.vault_storage_write_capacity}"

  attribute = [
    {
      name = "Path"
      type = "S"
    },
    {
      name = "Key"
      type = "S"
    }
  ]

  point_in_time_recovery { enabled = true }
  server_side_encryption { enabled = true }
  tags = "${merge(local.tags, var.tags)}"
}
