locals {
  tags = {
    Project     = "${var.project}"
    Environment = "${var.env}"
    Managed_by  = "terraform"
  }
}

resource "aws_s3_bucket" "terraform_state" {
  count = "${var.create_bucket ? 1 : 0}"

  bucket = "${var.s3_name}"
  acl    = "${var.acl}"

  versioning { enabled = "${var.versioning}" }
  tags = "${merge(local.tags, var.tags, map("Name", var.s3_name))}"
}

resource "aws_dynamodb_table" "terraform_lock" {
  count = "${var.create_table ? 1 : 0}"

  name           = "${var.dynamodb_table_name}"
  hash_key       = "LockID"
  read_capacity  = 1
  write_capacity = 1

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = "${merge(local.tags, var.tags, map("Name", var.dynamodb_table_name))}"
}
