#------------------------------------------------------------------------------
# general
#------------------------------------------------------------------------------
variable env {}
variable project {}
variable tags { type = "map", default = {} }

#------------------------------------------------------------------------------
# storage
#------------------------------------------------------------------------------
variable acl { default = "private" }
variable create_bucket { default = false }
variable create_table { default = false }
variable s3_name {}
variable dynamodb_table_name {}
variable versioning { default = true }

#------------------------------------------------------------------------------
# outputs
#------------------------------------------------------------------------------
output s3_arn    { value = "${aws_s3_bucket.terraform_state.*.arn[0]}" }
output table_arn { value = "${aws_dynamodb_table.terraform_lock.*.arn[0]}" }
